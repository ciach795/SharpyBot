﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;

namespace Sharpy.Classes
{
    public class Embeds
    {
        public static async Task NotAnOwnerEmbed(CommandContext ctx)
        {
            DiscordEmbedBuilder emb = new DiscordEmbedBuilder()
                .SetTitle("Error")
                .SetDescription("You are not an owner of this bot.")
                .SetColor(DiscordColor.Red);
            await ctx.RespondAsync(embed: emb);
        }
    }
}

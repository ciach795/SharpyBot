﻿using System;
using System.Diagnostics.CodeAnalysis;
using DSharpPlus.Entities;

namespace Sharpy.Classes
{
    public static class Embed
    {
        public static DiscordEmbedBuilder SetTitle(this DiscordEmbedBuilder embed, string title)
        {
            embed.WithTitle(title);
            return embed;
        }

        public static DiscordEmbedBuilder SetDescription(this DiscordEmbedBuilder embed, string description)
        {
            embed.WithDescription(description);
            return embed;
        }

        public static DiscordEmbedBuilder SetAuthor(this DiscordEmbedBuilder embed, string author, string url, string iconUrl)
        {
            embed.WithAuthor(author, url: url, iconUrl: iconUrl);
            return embed;
        }

        public static DiscordEmbedBuilder SetFooter(this DiscordEmbedBuilder embed, string footer, string iconUrl)
        {
            embed.WithFooter(footer, iconUrl);
            return embed;
        }

        public static DiscordEmbedBuilder SetImage(this DiscordEmbedBuilder embed, Uri imageUrl)
        {
            embed.WithImageUrl(imageUrl);
            return embed;
        }

        public static DiscordEmbedBuilder SetThumbnail(this DiscordEmbedBuilder embed, Uri thumbnailUrl)
        {
            embed.WithThumbnail(thumbnailUrl);
            return embed;
        }

        public static DiscordEmbedBuilder SetUrl(this DiscordEmbedBuilder embed, Uri url)
        {
            embed.WithUrl(url);
            return embed;
        }

        public static DiscordEmbedBuilder SetTimestamp(this DiscordEmbedBuilder embed, DateTimeOffset? time)
        {
            embed.WithTimestamp(time);
            return embed;
        }

        public static DiscordEmbedBuilder SetColor(this DiscordEmbedBuilder embed, DiscordColor color)
        {
            embed.WithColor(color);
            return embed;
        }
    }
}
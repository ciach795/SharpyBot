﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Sharpy;
using Sharpy.Classes;
using Sharpy.Models;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using MongoDB.Driver;

namespace Sharpy.Commands
{
    class ConfigCommands : BaseCommandModule
    {
        Database database;
        [Command("prefix"), Description("Sets the prefix."), RequirePermissions(Permissions.ManageGuild)]
        public async Task PrefixCommand(CommandContext ctx, string prefix)
        {
            database = new Database();
            var col = database.GetDatabase("Sharpy").GetCollection<GuildModel>("Config");
            try
            {
                var filter = Builders<GuildModel>.Filter.Eq("GuildId", ctx.Guild.Id);
                var update = Builders<GuildModel>.Update.Set("Prefix", prefix);
                await col.UpdateOneAsync(filter, update);
                var emb = new DiscordEmbedBuilder()
                    .SetTitle("✅ Success!")
                    .SetDescription($"Successfully updated the prefix to `{prefix}`.")
                    .SetColor(DiscordColor.Green)
                    .Build();
                await ctx.RespondAsync(embed: emb);
            } catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}

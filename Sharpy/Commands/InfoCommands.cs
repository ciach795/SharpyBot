﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Sharpy.Classes;

namespace Sharpy.Commands
{
    class InfoCommands : BaseCommandModule
    {
        [Command("info"), Aliases("botinfo"), Cooldown(1, 3, CooldownBucketType.User)]
        public async Task InfoCommand(CommandContext ctx)
        {
            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                .SetTitle("Information about Sharpy")
                .SetColor(DiscordColor.Green)
                .AddField("Versions", $"DSharpPlus: **{typeof(DSharpPlus.DiscordClient).Assembly.GetName().Version}**" +
                                       $"\n.NET: **{Environment.Version}**", inline: true);
            await ctx.RespondAsync(embed: embed.Build());
        }
        [Command("userinfo"), Description("Info about a user."), Cooldown(1, 3, CooldownBucketType.User)]
        public async Task UserinfoCommand(CommandContext ctx, DiscordMember user)
        {
            if (user == null) user = ctx.Member;
            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
            {
                Author =
                {
                    Name = $"{user.Username}#{user.Discriminator} - Information",
                    IconUrl = user.GetAvatarUrl(ImageFormat.Auto)
                },
                Footer =
                {
                    Text = $"Invoked by {user.Username}#{user.Discriminator}",
                    IconUrl = user.GetAvatarUrl(ImageFormat.Auto)
                }
            };
            if (user.Nickname != null) embed.AddField("Nickname", user.Nickname);
            await ctx.RespondAsync(embed: embed.Build());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Sharpy.Classes;

namespace Sharpy.Commands
{
    class ModerationCommands : BaseCommandModule
    {
        [Command("kick"), Description("Kicks a member."), RequirePermissions(Permissions.KickMembers)]
        public async Task KickCommand(CommandContext ctx, DiscordMember member,
            [RemainingText] string reason = "No reason.")
        {
            try
            {
                await member.RemoveAsync($"{ctx.Message.Author.Username}#{ctx.Message.Author.Discriminator} - {reason}");
                DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                    .SetTitle("Success!")
                    .SetDescription($"Kicked {member.Username}#{member.Discriminator} for {reason}")
                    .SetColor(DiscordColor.Green);
                await ctx.RespondAsync(embed: embed.Build());
            }
            catch (Exception exception)
            {
                DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                    .SetTitle("Error")
                    .SetDescription($"An error has occurred and I couldn't kick that member.\nError:\n{exception.Message}")
                    .SetColor(DiscordColor.Red);
                await ctx.RespondAsync(embed: embed.Build());
            }
        }

        [Command("ban"), Description("Bans a member."), RequirePermissions(Permissions.BanMembers)]
        public async Task BanCommand(CommandContext ctx, DiscordMember member,
            [RemainingText] string reason = "No reason.")
        {
            try
            {
                await member.BanAsync(0, $"{ctx.Message.Author.Username}#{ctx.Message.Author.Discriminator} - {reason}");
                DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                    .SetTitle("Success!")
                    .SetDescription($"Banned {member.Username}#{member.Discriminator} for {reason}")
                    .SetColor(DiscordColor.Green);
                await ctx.RespondAsync(embed: embed.Build());
            }
            catch (Exception exception)
            {
                DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                    .SetTitle("Error")
                    .SetDescription($"An error has occurred and I couldn't ban that member.\nError:\n{exception.Message}")
                    .SetColor(DiscordColor.Red);
                await ctx.RespondAsync(embed: embed.Build());
            }
        }
        [Command("space"), Description("Replaces all hyphens and underscores with a space."), RequirePermissions(Permissions.ManageChannels), Aliases("no-dash")]
        public async Task SpaceCommand(CommandContext ctx, DiscordChannel channel = null)
        {
            if (channel == null) channel = ctx.Channel;
            /*DiscordEmbedBuilder discordEmbedBuilder = new DiscordEmbedBuilder
            {
                Title = "⚠ Warning!",
                Description = "This space character is bugged on iOS and it is displayed as a question mark. Do you really want to replace the hyphens with this character? Reply with **yes** or **confirm** to continue.",
            };
            discordEmbedBuilder.SetFooter("You have 3 seconds to reply.", null);
            await ctx.RespondAsync(embed: discordEmbedBuilder.Build());
            var result = await ctx.Message.GetNextMessageAsync(m =>
            {
                return m.Content.ToLower() == "yes";
            });
            if (!result.TimedOut)
            {
                await ctx.RespondAsync("OK, I will do my things.");
                await channel.ModifyAsync(c => {
                    c.Name = channel.Name.Replace("-", "channelname");
                });
            } else
            {
                await ctx.RespondAsync("Timed out.");
            }*/
            await ctx.RespondAsync("Respond with *confirm* to continue.");
            var result = await ctx.Message.GetNextMessageAsync(m =>
            {
                return m.Content.ToLower() == "confirm";
            });

            if (!result.TimedOut) await ctx.RespondAsync("Action confirmed.");
        }
    }
}

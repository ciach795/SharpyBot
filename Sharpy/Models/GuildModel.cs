﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;

namespace Sharpy.Models
{
    public class GuildModel
    {
        public ObjectId Id { get; set; }
        public UInt64 GuildId { get; set; }
        public string Prefix { get; set; }
    }
}

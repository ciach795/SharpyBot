﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using Sharpy.Classes;

namespace Sharpy.Events.CommandsNext
{
    public class CommandExecuted
    {
        public static Task CommandExecutedTask(CommandsNextExtension s, CommandExecutionEventArgs cmd)
        {
            var user = cmd.Context.Message.Author;
            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                .SetTitle("Command executed")
                .SetDescription($"Command name: **{cmd.Command.Name}**\nAuthor: **{user.Username}#{user.Discriminator} ({user.Id})**")
                .SetColor(DiscordColor.Green);
            return Task.CompletedTask;
            //await s.Client.SendMessageAsync(await s.Client.GetChannelAsync(769604435642941481), embed: embed);
        }
    }
}
﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.CommandsNext.Exceptions;
using DSharpPlus.Entities;
using Sharpy.Classes;

namespace Sharpy.Events.CommandsNext
{
    public class CommandErrored
    {
        public static async Task CommandErroredTask(CommandsNextExtension s, CommandErrorEventArgs cmd)
        {
            switch (cmd.Exception)
            {
                case ChecksFailedException exc:
                    switch (exc.FailedChecks.FirstOrDefault())
                    {
                        case RequirePermissionsAttribute attr:
                            var perms = attr.Permissions.ToString();
                            DiscordEmbedBuilder emb = new DiscordEmbedBuilder()
                                .SetTitle("Error")
                                .SetDescription($"You are missing the following permissions:\n{perms}")
                                .SetColor(DiscordColor.Red);
                            await cmd.Context.RespondAsync(embed: emb.Build());
                            break;
                        case CooldownAttribute attr:
                            DiscordEmbedBuilder embed = new DiscordEmbedBuilder()
                                .SetTitle("⏱ You're on cooldown!")
                                .SetDescription($"Hey there! I saw that you're using the `{cmd.Command.Name}` command too fast! Please wait additional {attr.GetRemainingCooldown(cmd.Context).TotalSeconds} seconds before using that command.")
                                .SetColor(DiscordColor.Red);
                            await cmd.Context.RespondAsync(embed: embed.Build());
                            break;
                    }
                    break;
                case ArgumentException exc:
                    DiscordEmbedBuilder em = new DiscordEmbedBuilder()
                        .SetTitle("Error")
                        .SetDescription($"You have just passed not enough arguments.\nRequired arguments: \n{string.Join(",\n", cmd.Command.Overloads.FirstOrDefault().Arguments.Where(x => !x.IsOptional).Select(x => $"`{x.Name}` (type of `{x.Type.Name}`)"))}")
                        .SetColor(DiscordColor.Red);
                    await cmd.Context.RespondAsync(embed: em.Build());
                    break;
            }
            Console.WriteLine(cmd.Exception);
        }
    }
}
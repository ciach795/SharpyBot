﻿using System;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using Emzi0767.Utilities;
using MongoDB.Driver;
using Sharpy.Classes;
using Sharpy.Models;

namespace Sharpy.Events
{
    public class GuildCreatedAsync
    {
        private static Database _database;
        public static async Task GuildCreated(DiscordClient client, GuildCreateEventArgs evt)
        {
            try
            {
                Console.WriteLine("New Guild created!");
                /*var embed = new DiscordEmbedBuilder()
                    .SetTitle("➕ New Guild!")
                    .SetDescription("I was just added to a new Guild!")
                    .SetTimestamp(DateTimeOffset.Now);*/
                //await client.GetChannelAsync(789510693392875580).ConfigureAwait(true).GetAwaiter().GetResult().SendMessageAsync(embed: embed.Build());
                _database = new Database();
                var db = _database.GetDatabase("Sharpy");
                var col = db.GetCollection<GuildModel>("Config");
                var filter = Builders<GuildModel>.Filter.Eq("GuildId", evt.Guild.Id);
                var doc = col.Find(filter).FirstOrDefault();
                if (doc == null)
                {
                    Console.WriteLine($"Doc is null, inserting a document fo a new Guild!");
                    var x = new GuildModel
                    {
                        GuildId = evt.Guild.Id,
                        Prefix = "s:"
                    };
                    await col.InsertOneAsync(x);
                    Console.WriteLine($"Inserted a document for a new Guild!");
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc);
            }
        }
    }
}
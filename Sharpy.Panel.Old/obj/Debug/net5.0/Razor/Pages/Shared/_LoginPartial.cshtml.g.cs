#pragma checksum "/home/ciach/RiderProjects/Sharpy/Sharpy.Panel/Pages/Shared/_LoginPartial.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "00ec873817b02c57ce34be451ec2d58c5c0767b4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(Sharpy.Panel.Pages.Shared.Pages_Shared__LoginPartial), @"mvc.1.0.view", @"/Pages/Shared/_LoginPartial.cshtml")]
namespace Sharpy.Panel.Pages.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/home/ciach/RiderProjects/Sharpy/Sharpy.Panel/Pages/_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/home/ciach/RiderProjects/Sharpy/Sharpy.Panel/Pages/_ViewImports.cshtml"
using Sharpy.Panel;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"00ec873817b02c57ce34be451ec2d58c5c0767b4", @"/Pages/Shared/_LoginPartial.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"869340daffb25e2f1faee5ef80a5f2f081625714", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Shared__LoginPartial : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<ul class=\"navbar-nav ml-auto\">\r\n");
#nullable restore
#line 2 "/home/ciach/RiderProjects/Sharpy/Sharpy.Panel/Pages/Shared/_LoginPartial.cshtml"
     if (User?.Identity?.IsAuthenticated ?? false)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <li class=\"nav-item dropdown\">\r\n            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-expanded=\"false\">\r\n                ");
#nullable restore
#line 6 "/home/ciach/RiderProjects/Sharpy/Sharpy.Panel/Pages/Shared/_LoginPartial.cshtml"
           Write(User.Identity.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("#");
#nullable restore
#line 6 "/home/ciach/RiderProjects/Sharpy/Sharpy.Panel/Pages/Shared/_LoginPartial.cshtml"
                               Write(User.FindFirst("urn:discord:discriminator").Value);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
            </a>
            <ul class=""dropdown-menu bg-dark static-backdrop"" id=""dropdown"" aria-labelledby=""navbarDropdown"">
                <li>
                    <a class=""dropdown-item text-white"" href=""/guilds"">
                        <i class=""fas fa-tasks""></i>
                        Dashboard
                    </a>
                </li>
                <li>
                    <hr class=""dropdown-divider"">
                </li>
                <li>
                    <a class=""dropdown-item"" href=""/logout"">
                        <i class=""fas fa-sign-out-alt""></i>
                        Logout
                    </a>
                </li>
            </ul>
        </li>
");
#nullable restore
#line 26 "/home/ciach/RiderProjects/Sharpy/Sharpy.Panel/Pages/Shared/_LoginPartial.cshtml"
    }
    else
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <li class=\"nav-item\">\r\n            <a class=\"nav-link\" href=\"/login\">\r\n                Login\r\n                <i class=\"fas fa-sign-in-alt\"></i>\r\n            </a>\r\n        </li>\r\n");
#nullable restore
#line 35 "/home/ciach/RiderProjects/Sharpy/Sharpy.Panel/Pages/Shared/_LoginPartial.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</ul>
<style>
    .dropdown-item:not(.text-danger) {
        color: #c2c2c2 !important;
    }
    .dropdown-item:hover, .dropdown-item:active, .dropdown-item:focus {
        background: transparent!important;
        color: #ffffff!important;
    }
</style>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
